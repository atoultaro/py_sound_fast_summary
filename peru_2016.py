# -*- coding: utf-8 -*-
"""
Peru, 2016

Created on Wed Nov 28 12:15:39 2018

@author: ys587
"""
from __future__ import print_function
import glob, os
import numpy as np
#import pandas as pd
import logging
from numpy.random import RandomState
import time
from sklearn.cluster import MiniBatchKMeans, KMeans
import matplotlib.pyplot as plt
import sys
import re
import datetime as dt
fmt = '%Y%m%d'
import matplotlib.dates as mdates
#import soundfile as sf
import config

from clustering_utility import ReadSelFeaSite, DistWithinClass, MagAdjustedFea

#from dsp.SampleStream import SampleStream, Streams
#from dsp.abstractstream import StreamGap, StreamEnd
sys.path.append(os.path.abspath("../upcall-basic-net/upcall"))
sys.path.append(os.path.abspath("../upcall-basic-net"))
sys.path.append(os.path.abspath("../dsp"))
#from upcall.run_detector_dsp import make_sound_stream
import run_detector_dsp
import feature_extract as fe

#PROJ = 1 # HOG
PROJ = 2 # MFCC

config = config.Config()
##############################################################################
# Feture extraction
##############################################################################

for ss in config.site_list:
    site_sound_path = os.path.join(config.sound_path, ss)
    day_sound_path_list = os.listdir(site_sound_path)
    #for ff in range(len(day_sound_path_list)):
    #    day_sound_path_list[ff] = os.path.join(site_sound_path, day_sound_path_list[ff])
    #stream = make_sound_stream(day_sound_path_list)
    stream = run_detector_dsp.make_sound_stream(site_sound_path)

    ReturnedStr = fe.detect_power_law(stream, config.seltab_path, config)





if False:
    for dd in range(NumDay):    
        for ss in range(NumOfSite):
            if ss+1 < 10:
                FileSiteHead = DeployName+'_S0'+str(ss+1)
            else:
                FileSiteHead = DeployName+'_S'+str(ss+1)
            SiteDayListFlat.append(os.path.join(SitePathBase, FileSiteHead, FileSiteHead+'_'+DayList[dd]))
            
    SelTabPathList = [SelTabPath] * len(SiteDayListFlat)











##############################################################################
# Unsupervised learning
##############################################################################

SegLength = 30 # in min        
NumOfTimeSlots = int(24*(60./SegLength))
NumOfSec = SegLength*60
    
if PROJ == 1: # HOG
    FeaName = r'Hog'
    NumFreqBand = 20 # NumCellTime:2 ; NumCellFreq: 10
    NumBin = 18
    NumClass = 1000
    NumSite = 10    
    # Read day folders path
    DayList1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*.txt')))
    DayListFea1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*_Hog.npy')))
elif PROJ == 2: # MFCC
    FeaName = r'Mfcc'
    DayList1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*.txt')))
    DayListFea1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*_MFCC.npy')))
    NumClass = 1000
    
## Read selection tables & features
# Month 1
logging.warning('Late Feb to Early Apr, 2017')
#SelMetaTot1, SelFeaTot1 = ReadSelFeaSite(DayList1, DayListFea1, 30, 0)
SelMetaTot1, SelFeaTot1 = ReadSelFeaSite(DayList1, DayListFea1, 1, 0) # read all

logging.warning(' Revising features...')
SelFeaTot = SelFeaTot1
#SelFeaTot = np.vstack([SelFeaTot1, SelFeaTot2])
del SelFeaTot1
SelMetaTot = SelMetaTot1
#SelMetaTot = pd.concat([SelMetaTot1, SelMetaTot2])
del SelMetaTot1

## Magnitude adjusted
if PROJ == 1: # HOG
    SelFeaTot = MagAdjustedFea(SelFeaTot, NumFreqBand)


# load and classify
# Clustering via Mini-batch
logging.warning(' Minibatch kmeans...')

# data normalization
SelFeaTot0 = SelFeaTot
for d in range(SelFeaTot0.shape[1]):
    TargetDim = SelFeaTot0[:,d]
    TargetDim = TargetDim - TargetDim.mean()
    SelFeaTot[:,d] = TargetDim/TargetDim.std()

rng = RandomState(0)
TimeStart = time.time()
## Minibatch kmeans
##NumClass = 1000 # temporary change
#MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=rng)
#MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=0)
#MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=200, random_state=0)
MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=0.0, batch_size=40, random_state=0, verbose=1)
#MiniBatchClass.fit(SelMetaTrain[:,:48])
    
MiniBatchClass.fit(SelFeaTot)
train_time = (time.time() - TimeStart)
print("done in %0.3fs" % train_time)
# get the centroid
#FeaComponents = MiniBatchClass.cluster_centers_
FeaCounts = MiniBatchClass.counts_

# prediction: no need since compute_labels is true as default
LabelPred =  MiniBatchClass.labels_
SelMetaTot['Class'] = LabelPred
          
ClusterDist = MiniBatchClass.transform(SelFeaTot)

##############################################################################
# Distance of Centroids
Centroids = MiniBatchClass.cluster_centers_ 
#Label = MiniBatchClass.labels_
Inertia = MiniBatchClass.inertia_


##############################################################################    
## histogram
HistCount1, HistBin1 = np.histogram(LabelPred, bins=np.arange(-.5,float(NumClass)+.5, 1.0))
plt.figure(); plt.plot(HistBin1[:-1]+0.5, HistCount1,'-+'); plt.grid(); plt.show()


############################################################################################################
# Average distance of samples to their respective centroids
DistWithinClassInertia, DistWithinClassAvg = DistWithinClass(ClusterDist, LabelPred, NumClass)

############################################################################################################
# Distance between centroids
# empty class: 636, 648, 755, 904
############################################################################################################
from scipy.spatial.distance import pdist, squareform
DisMat2_Condensed = pdist(Centroids) # condensed distance matrix, given feature data
DisMat2 = squareform(DisMat2_Condensed) # a distance matrix, given a redudant one

if False:
    DisMat = np.zeros((NumClass, NumClass))
    for c1 in range(NumClass):
        for c2 in range(NumClass):
            DisMat[c1, c2] = np.sqrt(((Centroids[c1]-Centroids[c2])**2.).sum())
    DisMat[395, 229] # 83.177

############################################################################################################
# Dendrogram
# How to adjust threshold to get groups of clusters?

from scipy.cluster.hierarchy import dendrogram, linkage

#Z = linkage(Centroids, method='average')
Z = linkage(DisMat2_Condensed, method='average')
#Z = linkage(DisMat2, method='single')
#Z = linkage(DisMat2, method='centroid')

plt.figure()
t1 = dendrogram(Z, leaf_font_size=8)
plt.show()

#!!
# Task 1: DONE!!
# Merge classes based on disance
plt.figure()
fancy_dendrogram(
    Z,
    truncate_mode='lastp',
    p=100,
    leaf_rotation=90.,
    leaf_font_size=10.,
    show_contracted=True,
    annotate_above=10,
    max_d=13.0,  # plot a horizontal cut-off line
)
plt.show()

# Order class based on its distance with others
# ClassIdDist
# Given Z, output a matrix with class ID and distance in the decreasing order
ClassCount = 0
ClassIdDist = np.zeros((NumClass, 2))
for cc in range(Z.shape[0]):
    TargetZ = Z[-(cc+1)]
    if(TargetZ[0]<NumClass):
        ClassIdDist[ClassCount, 0] = TargetZ[0]
        ClassIdDist[ClassCount, 1] = TargetZ[2]
        ClassCount += 1
    if(TargetZ[1]<NumClass):
        ClassIdDist[ClassCount, 0] = TargetZ[1]
        ClassIdDist[ClassCount, 1] = TargetZ[2]
        ClassCount += 1
    
        


if False:
    from scipy.cluster.hierarchy import fcluster
    max_d = 10.0
    clusters = fcluster(Z, max_d, criterion='distance')
    clusters



############################################################################################################
sys.exit()