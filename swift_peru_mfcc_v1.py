#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""

V2 over V1: remove the image extration part, to reduce the memory usage

Extract images for learning representation in tensorflow
3 datasets: (i) __CHAOZX (ii) __NewZealand (iii) __SSW

cropped images / data augmentation / scale?

output: (i) selection table; (ii) images or waveform?

Goals: 
1. display long-term dataset
2. display short-term acoustic activity such as seiemic airgun on 20130902
periods of airgun pulses: 4 sec, 1 min. 
period of "gunshot" pulses: 3 min

Created on Tue Oct 24 14:10:31 2017

@author: atoultaro
"""
import numpy as np
import glob, os, sys
from math import pi as Pi
import logging
import scipy.signal as signal
import librosa
#from multiprocessing import Pool #Process, Queue,
#import itertools
import datetime
from soundfile import SoundFile
import multiprocessing as mp

sys.path.append(os.path.abspath("../dsp"))
sys.path.append(os.path.abspath("../upcall-net"))

from dsp.SampleStream import SampleStream, Streams
from dsp.abstractstream import StreamGap, StreamEnd
from upcall.run_detector_dsp import make_sound_stream, get_start_timestamp, fft_sample_to_spectro

from itertools import repeat

import pandas as pd
#from pyswift_core import GetTimeStamp, SoundRead, PowerLawMatCal, FindIndexNonZero, universal_worker, pool_args, MFCCFeaCalc, HogFeaCalc, MFCCTimeFeaCalc
#from pyswift_core import GetTimeStamp, SoundRead

class Config(object):
    SAMPLE_RATE = 48000
    FRAME_STEP_SEC = 1.0 # sec
    FRAME_SIZE_SEC = 2.0 # each window is 2 sec long
    
    FFT_SIZE = 4096
    WIN_SIZE = FFT_SIZE
    HOP_SIZE = 2400 # 50 msec
    IMG_F = 40 # 40, freq, IMG_X
    IMG_T = 40 # time, IMG_Y
    IMG_F_START, IMG_T_START = 5, 0

    NUM_MEL = 20

    MAX_STREAMS = int(3600./FRAME_STEP_SEC) #an hour =60.*60/1.
    #MAX_STREAMS = int(3600.*2./FRAME_STEP_SEC) #two hours
    #MAX_STREAMS = 36000 # .1 sec step for an hour =60.*60/.1
    #MAX_STREAMS = 18000 # 30 min
    #MAX_STREAMS = 9000 # 15 min
    #MAX_STREAMS = 6000 # 10 min
    #MAX_STREAMS = 3000 # 5 min
    #MAX_STREAMS = 600 # 1 min
    #MAX_STREAMS = 864000
    #MAX_STREAMS = 5000 # test
    #############################
    # Parallel
    # 0: no parallel computing is used
    # 1: parallel computing using multiple cores of CPU
    # 2: parallel computing using GPU
    PARALLEL_NUM = 1
    # Number of processes: the maximum is: 2 x number of CPUs - 1.
    NUM_CORE = 6

    def __init__(self):
        self.FRAME_STEP = int(self.FRAME_STEP_SEC*self.SAMPLE_RATE)
        self.FRAME_SIZE = int(self.FRAME_SIZE_SEC*self.SAMPLE_RATE)
    
def mfcc_fea_calc(samples, config):
    mfcc_seq = librosa.feature.mfcc(y=samples, sr=config.SAMPLE_RATE, n_mfcc=config.NUM_MEL)
    return np.concatenate((mfcc_seq.mean(axis=1), mfcc_seq.std(axis=1)))

def mfcc_fea_calc_diff(samples, config):
    mfcc_seq = librosa.feature.mfcc(y=samples, sr=config.SAMPLE_RATE, n_mfcc=config.NUM_MEL)
    mfcc_seq_diff = np.diff(mfcc_seq)
    return np.concatenate((mfcc_seq.mean(axis=1), mfcc_seq.std(axis=1), mfcc_seq_diff.mean(axis=1), mfcc_seq.std(axis=1)))

def make_sound_stream_max_history(site_sound_path, format_str = "%Y%m%d_%H%M%S", max_history=96000):
    stream_elements = Streams()
    
    # if it's not a list make it a list
    if isinstance(site_sound_path, (list,)) is False:
        site_sound_path = [site_sound_path]
    
    for ii in range(len(site_sound_path)):
        # get the file director
        file_dir = site_sound_path[ii]

        # Iterate through the folders and extract associated
        for filename in os.listdir(file_dir):
            
            # if soundfile add it to the stream
            if filename.endswith(".wav") or filename.endswith(".aif") or \
            filename.endswith("flac"): 
                sound_fullfile = file_dir + '/' + filename
                start = get_start_timestamp(filename, format_str)
                aa = SoundFile(sound_fullfile)
                stream_elements.add_file(sound_fullfile, [len(aa)], 
                                         [start], aa.samplerate)
                # print(os.path.join(directory, filename)) # debugging
            else:
                continue
                
    # Combine streams into a sample stream
    stream = SampleStream(stream_elements, max_history)
        
    return stream

def multi_processing_fft(samp_list, config):
    pool_fft = mp.Pool(processes=config.NUM_CORE)
    results = pool_fft.starmap(fft_sample_to_spectro, zip(samp_list, repeat(config)))
    fea_list = []
    for result in results:
        fea_list += result
    pool_fft.close()
    pool_fft.join()
    
    return fea_list

def multi_processing_mfcc(samp_list, config):
    pool_fft = mp.Pool(processes=config.NUM_CORE)
    #results = pool_fft.starmap(mfcc_fea_calc, zip(samp_list, repeat(config)))
    results = pool_fft.starmap(mfcc_fea_calc_diff, zip(samp_list, repeat(config)))
    
    fea_array = np.vstack(results)
    #fea_list = []
    #for result in results:
    #    fea_list.append(result)
    pool_fft.close()
    pool_fft.join()
    
    return fea_array

def fea_to_pickle(fea_list, timestamp_array, file_array, seltab_path):
    fea_arr = np.vstack(fea_list)
    fea_pd = pd.DataFrame(fea_arr)
    fea_pd = fea_pd.join(pd.Series(timestamp_array, name='timestamp'))
    fea_pd = fea_pd.join(pd.Series(file_array, name='file'))
    fea_pd_pickled = os.path.join(seltab_path, timestamp_array[0].strftime("%Y%m%d_%H%M%S")+'.pkl')
    fea_pd.to_pickle(fea_pd_pickled)
    
    return fea_pd

def extract_feature(site_sound_path, seltab_path, config):
    site_sound_path = site_sound_path.replace('\\','/')
    
    seltab_path = seltab_path.replace('\\','/')
    if not os.path.exists(seltab_path):
        os.makedirs(seltab_path)
    
    # make sound stream
    stream = make_sound_stream_max_history(site_sound_path)
    
    print('Extract features from sound files...')
    sec_advance = float(config.FRAME_SIZE/stream.get_all_stream_fs()[0])
    
    counter = 0
    samp_list = []
    fea_list = []
    file_array = []
    timestamp_array = []
    fea_pd_list = []
    #previous_channels = stream.stream.channels
    #current_channels = stream.stream.channels
    
    while True:
        try:
            # load the samples
            samps = stream.read(N=config.FRAME_STEP, previousN=config.FRAME_SIZE-config.FRAME_STEP)[0]
            samps = samps[:,-1] # librosa expectes single-channel audio as input
            samp_list.append(samps)
            
            #advance the counter
            counter +=1
            #print('Count: '+str(counter))

            timestamp = stream.get_current_timesamp() - datetime.timedelta(seconds=sec_advance)
            timestamp_array.append(timestamp) # Append the timestamp
            #print(timestamp)
            
            # Set the current file
            current_file = stream.stream.filename  
            file_array.append(current_file) # Set the current file             
            
            if counter == config.MAX_STREAMS:
                print("Extracting features...\n")
                if config.PARALLEL_NUM == 1: # parallel computing using CPU
                    #fea_list = multi_processing_fft(samp_list, config)
                    fea_list.append(multi_processing_mfcc(samp_list, config))
                    
                    print(timestamp)
                    if False: # save for each MAX_STREAMS, e.g. an hour
                        fea_pd_list.append(fea_to_pickle(fea_list, timestamp_array, file_array, seltab_path))
                        fea_list = []                    
                        file_array = []
                        timestamp_array =[]
                    
                    samp_list = []
                    counter = 0


        except (StreamGap, StreamEnd) as S:
            if type(S).__name__ == 'StreamEnd' :
                print('STREAM-END: save the extracted features into numpy array files.')
                #fea_list = multi_processing_fft(samp_list, config)
                fea_list.append( multi_processing_mfcc(samp_list, config))
                
                timestamp = stream.get_current_timesamp() - datetime.timedelta(seconds=sec_advance)
                timestamp_array.append(timestamp) # Append the timestamp
                print(timestamp)
                current_file = stream.stream.filename  
                file_array.append(current_file) # Set the current file             
                
                fea_pd_list.append(fea_to_pickle(fea_list, timestamp_array, file_array, seltab_path))

                return False
                
            elif type(S).__name__ == 'StreamGap':
                print('STREAM-GAP: save the extracted features into numpy array files.')
                #fea_list = multi_processing_fft(samp_list, config)
                fea_list.append(multi_processing_mfcc(samp_list, config))

                timestamp = stream.get_current_timesamp() - datetime.timedelta(seconds=sec_advance)
                timestamp_array.append(timestamp) # Append the timestamp
                print(timestamp)
                current_file = stream.stream.filename  
                file_array.append(current_file) # Set the current file             
                
                fea_pd_list.append(fea_to_pickle(fea_list, timestamp_array, file_array, seltab_path))
                fea_list = []
                file_array = []
                timestamp_array =[]
                
                samp_list = []
                counter = 0                
                
                continue
    
    fea_pd = pd.concat(fea_pd_list)
    fea_pd = fea_pd.reset_index(drop=True)
    
    return fea_pd

if __name__ == "__main__":    
    SitePathBase = r'/home/ys587/__Data/__Peru/__sound/'
    #SitePathBase = r'/home/ys587/__Data/__Peru/__sound_short' # test for shorter sound
    seltab_path = r'/home/ys587/__Data/__Peru/__seltab/'
    
    NumOfSite = 2
    
    site_day_list_flat = []
    for ss in range(NumOfSite):
        site_day_list_flat.append(os.path.join(SitePathBase, 'SK'+str(ss+1)))
    
    seltab_path_sk1 = os.path.join(seltab_path, 'sk1')
    seltab_path_sk2 = os.path.join(seltab_path, 'sk2')
    
    config = Config()
    
    the_day = 0
    fea_sk1 = extract_feature(site_day_list_flat[the_day], seltab_path_sk1, config)
    the_day = 1
    fea_sk2 = extract_feature(site_day_list_flat[the_day], seltab_path_sk2, config)

    fea_pd_tot = pd.concat([fea_sk1, fea_sk2])
    fea_pd_tot = fea_pd_tot.reset_index(drop=True)
