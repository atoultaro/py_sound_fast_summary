# -*- coding: utf-8 -*-
"""
(1) Align the audio files by the time codes in the filenames
(2) calculate the spectrum for each time resolution

Created on Fri Aug 25 17:51:40 2017

@author: ys587
"""
import re
import datetime
import glob
import os
import numpy as np

TIME_FORMAT = "%Y%m%d_%H%M%S" # this is how your timestamp looks like
regex = re.compile("_(\d{8}_\d{6})") # YYYYMMDD_HHMMSS

# For STFT
FreqReso = 10 # 10 Hz per point
# Hop length affects the time resolution
FsRef  = 32000
NumFFT = FsRef/FreqReso
NumFFTHalf = NumFFT/2 + 1

# For CQT
hop_length = 512
TimeScaleSec = 120. # 2 min
#TimeScaleSec = 30. # 30 sec
#TimeScaleSec = 1. # 1 sec
FMin = 30
NumOct = 9
MidiPerOct = 12

def GetTimeStamp(TheString):
    m = regex.search(TheString)
    return datetime.datetime.strptime(m.groups()[0], TIME_FORMAT)

if __name__ == "__main__":
    SoundOldPath = r'P:\projects\2016_BRP_IthacaNY_S1068\S1068_NZ01_201612\S1068_NZ01_TARU_FLAC'
    SoundNewPath = r'P:\users\yu_shiu_ys587\__Soundscape\__NewZealandData\S1068_NZ01_201612_Aligned'
    
    DeployName = 'S1068NZ01'
    DayList = ['20161209', '20161210', '20161211', '20161212', '20161213', '20161214', '20161215', '20161216', '20161217', '20161218']
    NumDay = len(DayList)
    NumOfSite = 10 
    
    # Check time after 20161209 23:45:01
    
    # align time
    SiteDayList = [[None for x in range(NumDay)] for y in range(NumOfSite)]
    for ss in range(NumOfSite):
        if ss+1 != NumOfSite:
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SoundOldPath, DeployName+'_S0'+str(ss+1), '*')))
        else: #SiteNum==10
            SiteDayList[ss] = sorted(glob.glob(os.path.join(SoundOldPath, DeployName+'_S'+str(ss+1), '*')))
    
    for ss in range(NumOfSite-1): # the site 10 is removed from analysis
        for dd in range(NumDay):
            print 'dd: ' + str(DayList[dd]),
            print 'ss: '+str(ss)
            FileList = sorted(glob.glob(os.path.join(SiteDayList[ss][dd]+'/','*')))
            for ff in range(len(FileList)):
                ff2 = os.path.splitext(os.path.basename(FileList[ff]))[0]
                TimeCurr = GetTimeStamp(ff2)

    