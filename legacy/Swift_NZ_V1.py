# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 09:32:55 2017

New Zealand Long-Term Spectrogram (CQT) for every site and day
# Memory issue: out of memory after 2 site figures 

To-do-jobs
1. long-term spectrogram or cqt-based spectrogram (freq vs day)
2. power-law-based onset events (hourly count vs day)
3. freq-band events (frer-band count vs time)

Steps:
1. Check the time from the filenames for all the units.
2. Find the common/ overlapped duration
3. draw individual STFT <<== start with this

Time: 
1. focus the dates between 20160210 - 20160219
2. 10 days!
3. UTC time: New Zealand time is UTC/GMT +12 hours for stanard time and +13 for daylight saving time
4. Time is not precise. Neet to find a way to store the FFT results

Challenges:
1. Need to find a way of selecting the expected days and time for the analysis and ignoring others

    
@author: ys587
"""

FLAG_FIG = True
#FLAG_DEBUG = True
TEST_SNGLE_FILE = False

import os, glob
import time
import sys
import soundfile as sfile
import librosa as rosa
import matplotlib.pyplot as plt
import numpy as np
import re
#from multiprocessing import Pool

# For STFT
FreqReso = 10 # 10 Hz per point
# Hop length affects the time resolution
FsRef  = 32000
NumFFT = FsRef/FreqReso
NumFFTHalf = NumFFT/2 + 1

# For CQT
hop_length = 512
TimeScale = int(30./(hop_length/float(FsRef))) # 30 sec
FMin = 30
NumOct = 9
MidiPerOct = 12

regex = re.compile("_(\d{8})$") # YYYYMMDD

def DaySpectroCalcCQTSimple(DayName, TimeScale):
    Samples, Fs = sfile.read(DayName)
    #Day_Spectrogram0 = abs(rosa.stft(Samples, n_fft = NumFFT, hop_length = Fs))
    Day_Spectrogram0 = abs(rosa.cqt(Samples, sr=Fs, hop_length=512, fmin=FMin, n_bins=NumOct*MidiPerOct, bins_per_octave=MidiPerOct))
    #Day_Spectrogram0 = abs(rosa.cqt(Samples, sr=Fs, hop_length=512, fmin=15, n_bins=10*6, bins_per_octave=6))
    #Day_Spectrogram0 = abs(rosa.cqt(Samples, sr=Fs, hop_length=512, fmin=120, n_bins=5*6, bins_per_octave=6))
    
    Day_Spectrogram_DimT = int(np.floor(Day_Spectrogram0.shape[1]/TimeScale))
    Day_Spectrogram = np.zeros([Day_Spectrogram0.shape[0], Day_Spectrogram_DimT])
    for ii in range(Day_Spectrogram_DimT):
        Day_Spectrogram[:,ii] = Day_Spectrogram0[:, ii*TimeScale:(ii+1)*TimeScale].mean(axis=1)
    return Day_Spectrogram
    #return Day_Spectrogram0
        
if __name__ == "__main__":
    SitePathBase = r'F:\S1068_NZ01_201612_UniformDays'
    VisOutputPath = r'N:\users\yu_shiu_ys587\__SoundScape\__NewZealand'
    
    SiteNameList = os.listdir(SitePathBase)
    
    for SiteName in SiteNameList:
    #for SiteName in SiteNameList[:1]: #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        #SitePath = r'C:\ASE_Data\__NewZealand\S1068NZ01_S01'
        ##SitePath = r'F:\S1068_NZ01_201612_UniformDays\S1068NZ01_S01'
        #SitePath = r'C:\ASE_Data\__NewZealand\S1068NZ01_S01\S1068NZ01_S01_20161211'
        #SitePath = SitePath.replace('\\','/')
        #SiteName = os.path.split(SitePath)[-1]
        SitePath = os.path.join(SitePathBase, SiteName)
        
        if FLAG_FIG:
            #fig, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
            fig, axarr = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True, figsize=(18.0, 9.0)) 
            fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
            fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')
        
        WorkList = sorted(glob.glob(os.path.join(SitePath+'/','*'))) 
        cc = 0
        for dd in range(len(WorkList)): # days in a single site
        #for dd in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
            WorkList[dd] = WorkList[dd].replace('\\','/')
            print WorkList[dd]
            DayList = sorted(glob.glob(os.path.join(WorkList[dd]+'/','*')))
            
            m = regex.search(os.path.split(WorkList[dd])[-1])
            NameYYYYMMDD = m.groups()[0]
        
            #t1 = time.time()
            #DaySpectrogramAbs = abs(DaySpectroCalc(DayList[0]))
            
            print "Calculating spectrogram..."
            DaySpectrogramAbs = []
            for ii in range(len(DayList)): # sound files in a single day
            #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
                #if ii % 10 == 0:
                if ii % 1 == 0:                    
                    print "Sound file from " + str(ii) + "..."
                #DaySpectrogramAbs.append(DaySpectroCalc(DayList[ii]))
                #DaySpectrogramAbs.append(DaySpectroCalcSimple(DayList[ii], TimeReso))
                DaySpectrogramAbs.append(DaySpectroCalcCQTSimple(DayList[ii], TimeScale))
                    
            print "Merging..."
            DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
            del DaySpectrogramAbs
            print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
            #print "DaySpectrogramAbsTot T shape: " + str((DaySpectrogramAbsTot.T).shape)
            
            if FLAG_FIG:
                #axarr[dd/2, dd%2].imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect='equal')
                Fs = sfile.info(DayList[dd]).samplerate
                #XYLim = [0.0, 24.0, 0.0, FsRef/2.0]
                #XYLim = [0.0, 24.0, FMin, FMin*(2.**NumOct)]
                #axarr[cc/3, cc%3].imshow(DaySpectrogramAbsTot**.1, origin='lower', extent=XYLim, aspect='auto') #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
                #xx = np.arange(0.0, 24.0)
                
                DimT = DaySpectrogramAbsTot.shape[1]
                xx = np.arange(0, DimT)*TimeScale*512./Fs/3600
                yy = FMin*2**(np.arange(0, (NumOct*1.0)*MidiPerOct)/MidiPerOct)
                axarr[cc/3, cc%3].pcolor(xx, yy, (DaySpectrogramAbsTot)**.1)
                axarr[cc/3, cc%3].set_yscale('log')
                axarr[cc/3, cc%3].set_title(NameYYYYMMDD)
                cc += 1
    
        #plt.show()

        #fig.savefig(os.path.split(SitePath)[-1]+".png") ##<<=== need add time
        #fig.savefig(SiteName+".png")
        # Colorbar
        
        #fig.subplots_adjust(right=0.8)
        #cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        #fig.colorbar(im, cax=cbar_ax)
        fig.savefig(os.path.join(VisOutputPath, SiteName+".png"))
    
    #MidFreqArr = MidFreqTable(20, 32) #
    #BandEdgeArr = BandEdgeTable(MidFreqArr)
    #FilterBankList = FilterBank(BandEdgeArr, Fs)
    #Meta = [FilterBankList, SegLen, TimeResolution]
        
    #print str(time.time()-t1)+ ' sec'
    
    #LongTermSpectrogram = LongTermSpectrogram(DayList)
                     
    if False:
        fig, axarr = plt.subplots(2, 2, sharex=True, figsize=(80, 60))
        axarr[0, 0].imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect='equal')
        #ax.imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect=.1)
        plt.show()
        #ax.colorbar()
        #ax.imshow(DaySpectrogramAbs, origin='lower', aspect=1. )

    
    
    
#    if TEST_SNGLE_FILE == True:
#        #OnsetDetection(DayList[11], SelTabPathList[11]) # kp11_20160430_000000    
#        #HarmonicsDetect(DayList[0], SelTabPathList[0]) # kp01_20141221_000000
#        ReturnedStr = LongTermSpectrogram(DayList[1], SelTabPathList[1]) #
#        print ReturnedStr


#    else:
#        MyPool = Pool(3) 
#        try:
#            #for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(HarmonicsDetect, DayList, SelTabPathList)):
#            #for ReturnedStr in MyPool.imap(universal_worker, pool_args(HarmonicsDetect, DayList, SelTabPathList)):
#            for ReturnedStr in MyPool.imap(PyBaleen.universal_worker, PyBaleen.pool_args(HarmonicsDetect, DayList[1:4], SelTabPathList[1:4])):
#                print ReturnedStr
#            #print "Waiting 10 seconds"
#            time.sleep(10)
#        except KeyboardInterrupt:
#            print "Caught KeyboardInterrupt, terminating workers"
#            MyPool.terminate()
#            MyPool.join()
#        else:
#            print "Work is finished. Quitting gracefully"
#            MyPool.close()
#            MyPool.join()


#def SignalFreqTime(DaySound, Meta):
#    FilterBank = Meta[0]
#    PageSize = Meta[1]
#    TimeReso = Meta[2]
#    # apply filter; calculate power on time segments
#    # PageSize needs to be integer multiple of TimeReso.
#    # Eg. PageSize = 10 min whereas TimeReso = 10 sec 
#    # Iteration over Samples or over filters?
#    Fs = sfile.info(DaySound).samplerate
#    NumTime = int(np.floor(float(sfile.info(DaySound).duration)/TimeReso))
#    #Samples0, Fs = sfile.read(DaySound)
#    NumPerPage = int(PageSize/TimeReso)
#    NoiseFreqTimeDayMap = np.zeros((len(FilterBank), NumTime ))
#    
#    tt = 0
#    for Samples0 in sfile.blocks(DaySound, blocksize = int(PageSize*Fs)):
#        #for ii in range(len(FilterBank)):
#        for ii in range(1):
#            Samples = sig.filtfilt(FilterBank[ii][0], FilterBank[ii][1], Samples0)
#            #for jj in range(NumPerPage):
#            for jj in range(1):
#                NoiseFreqTimeDayMap[ii, tt*NumPerPage+jj:(tt+1)*NumPerPage] = ((Samples[jj*TimeReso:(jj+1)*TimeReso]**2.).mean())**.5
#        tt += 1
        