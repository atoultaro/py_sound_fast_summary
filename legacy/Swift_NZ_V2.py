# -*- coding: utf-8 -*-
"""
Created on Wed Apr 05 09:32:55 2017

New Zealand: Sound count diel plot
(1) Diel plot of powers
(2) Diel plot of sound counts
(3) Diel plot of dominant frequency
(4) Diel plot of givin sound examplar, such as woodpecker or coyote

To-Do-List:
(1) save computed results such as CQT? 

@author: ys587
"""

FLAG_FIG = False
#FLAG_DEBUG = True
TEST_SNGLE_FILE = True

import os
import glob
import re
import datetime
import soundfile as sf
import matplotlib.pyplot as plt
import numpy as np
import librosa
import scipy.signal as signal
import time
from multiprocessing import Pool #Process, Queue,
#import parmap
import itertools

TIME_FORMAT = "%Y%m%d_%H%M%S" # this is how your timestamp looks like
regex = re.compile("_(\d{8}_\d{6})") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})$") # YYYYMMDD_HHMMSS
#regex1 = re.compile("_(\d{2})(\d{2})(\d{2})_") # YYYYMMDD_HHMMSS

# Sound path & selection table output path
#WorkPath = r'N:\projects\2006_Excelerate_MassBay_52965\52965_Dep20_20120328\52965_Dep20_AIFF'
#SelTabPath = r'N:\users\yu_shiu_ys587\__MassBay\__Dep20_2012\__LogHarm'

# Debug flags
FLAG_PLOT = 0

# Parameters
Nu1 = 2.0
Nu2 = 1.0
#Gamma = 1.0
Gamma = 1.0
SegLen = 60 # segment length for Power Law, 60 sec
#SampleRateRef = 2000
SampleRateRef = 16000 # New Zealand data
HopLength = 2**6
#TDetecThre = 0.15 # powerlaw value
TDetecThre = 1.2

EventTimeReso = HopLength*1.0/SampleRateRef
TDetecThreLen = 0.25 # in sec
TDetecThreLen = max([EventTimeReso*3.0, TDetecThreLen])

FreqMin = 50
BinsPerOctave = 12*1
OctaveNum = 7

BinTotal = BinsPerOctave*OctaveNum

Winn = np.hamming(21)
Winn = Winn/np.sum(Winn)

def GetTimeStamp(TheString):
    m = regex.search(TheString)
    return datetime.datetime.strptime(m.groups()[0], TIME_FORMAT)

def SoundRead(FileName):
    Samples, SampleRate = sf.read(FileName)
    #return Samples, SampleRate, NumChan, NumFrame
    return Samples, SampleRate

"""
def STFT_Yu(Samples0, WinSize, FFTSize, HopSize):
    TotalSegments = np.int32(np.ceil(len(Samples0) / np.float32(HopSize)))
    Window = np.hanning(WinSize)
    Proc = np.concatenate((Samples0, np.zeros(WinSize))) # the data to process
    Spectrogram = np.empty((TotalSegments, int(0.5*FFTSize)+1), dtype=float)
    for i in xrange(TotalSegments): # for each segment
        CurrentHop = HopSize * i  # figure out the current segment offset
        Segment = Proc[CurrentHop: CurrentHop+WinSize]  # get the current segment
        Windowed = Segment * Window  # multiply by the half cosine function
        Padded = np.append(Windowed, np.zeros(FFTSize-WinSize))  # add 0s to double the length of the data
        Spectrum = np.fft.fft(Padded)
        Autopower = np.abs(Spectrum * np.conj(Spectrum))  # find the autopower spectrum
        Spectrogram[i, :] = Autopower[0:int(0.5*FFTSize)+1]
    return Spectrogram
"""

def PowerLawMatCal(SpectroMat, Nu1, Nu2, Gamma):
    DimF, DimT = SpectroMat.shape
    Mu_k = [PoweLawFindMu(SpectroMat[ff,:]) for ff in range(SpectroMat.shape[0])]
    
    Mat0 = SpectroMat**Gamma - np.array(Mu_k).reshape(DimF,1)*np.ones((1, DimT))
    MatADenom = [(np.sum(Mat0[:,tt]**2.))**.5 for tt in range(DimT)]
    MatA = Mat0 / (np.ones((DimF,1)) * np.array(MatADenom).reshape(1, DimT) )
    MatBDenom = [ (np.sum(Mat0[ff,:]**2.))**.5 for ff in range(DimF)]
    MatB = Mat0 / (np.array(MatBDenom).reshape(DimF,1) * np.ones((1, DimT)))
    #PowerLawTFunc = np.sum((MatA**Nu1)*(MatB**Nu2), axis=0)
    PowerLawMat = (MatA**Nu1)*(MatB**Nu2)
    #return PowerLawTFunc
    return PowerLawMat
    
def PoweLawFindMu(SpecTarget):
    SpecSorted = np.sort(SpecTarget)
    SpecHalfLen = int(np.floor(SpecSorted.shape[0]*.5))
    IndJ = np.argmin(SpecSorted[SpecHalfLen:SpecHalfLen*2] - SpecSorted[0:SpecHalfLen])
    Mu = np.mean(SpecSorted[IndJ:IndJ+SpecHalfLen])
    return Mu

def FindIndexNonZero(DiffIndexSeq):
    IndSeq = []
    ItemSeq = []
    for index, item in enumerate(DiffIndexSeq):
        if(item != 0):
            IndSeq.append(index)
            ItemSeq.append(item)
    return IndSeq, ItemSeq

def DetectViaPowerLaw(DaySound, SelTabPath):
    DaySound = DaySound.replace('\\','/')
    #SoundList = sorted(glob.glob(os.path.join(DaySound+'/', '*.wav')))
    
    SelTabPath = SelTabPath.replace('\\','/')
    #DayFile = os.path.join(SelTabPath+'/',os.path.basename(DaySound)+'.txt')
    DayFile = os.path.join(SelTabPath+'/',os.path.splitext(os.path.basename(DaySound))[0]+'.txt')
    #DayFile = os.path.join(SelTabPath+'/',os.path.basename(SoundList)+'.txt')
    #if not os.path.exists(DayFile):
    if True:
        f = open(DayFile,'w')
        #f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\n')
        f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore')
        #for hh in range(BinTotal):
        for hh in range(BinTotal*2):
            f.write('\tFea'+str(hh))
        f.write('\n')
        EventId=0
        
        if True:        
        #for ff in SoundList:
        #for ff in DaySound:
        #for ff in SoundList[3:8]:
            ff = DaySound
            ff2 = os.path.splitext(os.path.basename(ff))[0]
            print ff2                    
            # time stamp
            TimeCurr = GetTimeStamp(ff2)
            print TimeCurr                    
            # read sound file
            Samples0, SampleRate0 = SoundRead(ff)
            # resampling 
            if (SampleRate0 != SampleRateRef):
                Samples0 = signal.resample(Samples0, float(len(Samples0))/SampleRate0*SampleRateRef)
            
            # make single-channel sound multi-dimensional array
            if(Samples0.ndim == 1):
                Samples0 = np.array([Samples0]).T
            
            for cc in range(Samples0.shape[1]):            
            #for cc in range(2, 5): # channel number
                print "Channel "+str(cc)
                
                for ss in range(int(np.floor(Samples0.shape[0]*1.0/SampleRateRef/SegLen))): # fixed the problem on SampleRate. Use SampleRate0 mistakenly at first.
                #for ss in range(2, 15, 5):
                    Samples = Samples0[ss*SegLen*SampleRateRef:(ss+1)*SegLen*SampleRateRef,cc]
                    Samples = Samples - Samples.mean()
                    
                    # Constant-Q
                    #BinsPerOctave = 12*2
                    CqtMat = librosa.cqt(Samples, hop_length=HopLength, bins_per_octave=BinsPerOctave, n_bins=BinTotal, sr=SampleRateRef, fmin=FreqMin) 
                    PowerLawCqt = PowerLawMatCal(CqtMat, Nu1, Nu2, Gamma)
                    PowerLawCqt = PowerLawCqt*(PowerLawCqt>0.0) # make sure PowerLawCqt0 is positive and so is its mean for each freq
                    PowerLawCqtMu = np.mean(PowerLawCqt, axis=1)
                    PowerLawCqtMu = signal.medfilt(PowerLawCqtMu, 3)
                    PowerLawCqtMuMat = np.array([PowerLawCqtMu]).T *np.ones([1, PowerLawCqt.shape[1]])
                    PowerLawCqt = PowerLawCqt - PowerLawCqtMuMat
                    PowerLawCqt = PowerLawCqt*(PowerLawCqt>=0)
                    PowerLawCqt = PowerLawCqt / PowerLawCqtMuMat
                    
#                    plt.matshow(PowerLawCqt0[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
#                    plt.matshow(PowerLawCqt1[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
#                    plt.matshow(PowerLawCqt2[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
#                    plt.matshow(PowerLawCqt[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
                    
                    #TDetecFunc0 = (np.sum(PowerLawCqt, axis=0))**2.0
                    #TDetecFunc0 = np.sum(PowerLawCqt, axis=0)/BinTotal
                    TDetecFunc0 = np.mean(PowerLawCqt, axis=0)
                    TDetecFunc = signal.medfilt(TDetecFunc0, 21) # <<=== median filter!
                    TDetecFunc = np.convolve(TDetecFunc, Winn, mode='same')
                    
                    TDetecRange = (TDetecFunc > TDetecThre).astype(int)
                    TDetecRangeDiff = np.diff(TDetecRange)
                    IndDiff, ValDiff = FindIndexNonZero(TDetecRangeDiff)
                    
                    if FLAG_PLOT:
                        Ta, Tb = 0, 20-1
                        plt.matshow(PowerLawCqt[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
                        plt.plot(TDetecFunc[Ta/EventTimeReso:Tb/EventTimeReso]); plt.show()
                        Ta, Tb = 20, 40-1
                        plt.matshow(PowerLawCqt[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
                        plt.plot(TDetecFunc[Ta/EventTimeReso:Tb/EventTimeReso]); plt.show()
                        Ta, Tb = 40, 60-1
                        plt.matshow(PowerLawCqt[:, Ta/EventTimeReso:Tb/EventTimeReso], origin='lower', cmap='jet'); plt.show()
                        plt.plot(TDetecFunc[Ta/EventTimeReso:Tb/EventTimeReso]); plt.show()
                    
                    EventList = []
                    if ValDiff: # test if not empty
                        if(ValDiff[0] == -1):
                            ValDiff = [1] + ValDiff
                            IndDiff = [0] + IndDiff
                        if(ValDiff[-1] == 1):
                            ValDiff.append(-1)
                            IndDiff.append(len(TDetecRangeDiff))
                        for tt in range(0, len(ValDiff), 2):
                            TimeE1 = IndDiff[tt]
                            TimeE2 = (IndDiff[tt+1]+1)
                            EventTVal = np.max(TDetecFunc[IndDiff[tt]:(IndDiff[tt+1]+1)]) # This is the score! It is the max value in the detection function.
                            
                            # FEATURE: FreqHist
                            FreqHist = np.mean(PowerLawCqt[:,TimeE1:TimeE2], axis=1)
                            FreqHist = FreqHist*(FreqHist>0)
                            FreqHist = FreqHist/np.sum(FreqHist)
                            if(TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso):
                                EventList.append([TimeE1,TimeE2,EventTVal, FreqHist])
                            
                            # FEATURE: FreqHistTime
                            if False:
                                TimeEMid = int(np.floor((TimeE1+TimeE2)*0.5))+1
                                FeaMat0 = np.mean(PowerLawCqt[:,TimeE1:TimeE2], axis=1)
                                FeaMat1 = np.mean(PowerLawCqt[:,TimeEMid:TimeE2], axis=1) - np.mean(PowerLawCqt[:,TimeE1:TimeEMid], axis=1)
                                if(TDetecThreLen <= (TimeE2-TimeE1)*EventTimeReso):
                                    EventList.append([TimeE1,TimeE2,EventTVal, FeaMat0, FeaMat1])
                                
                        # write to selection table
                        for ee in range(len(EventList)):
                            #print EventId
                            EventId += 1
                            Time1 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][0]*EventTimeReso
                            Time2 = TimeCurr.hour*3600 + TimeCurr.minute*60.0 + TimeCurr.second + ss*SegLen*1.0 + EventList[ee][1]*EventTimeReso
                            #f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t50\t350\t'+str.format("{0:<.4f}",EventList[ee][2])+'\n')
                            f.write(str(EventId)+'\t'+'Spectrogram'+'\t'+str(cc+1)+'\t'+str.format("{0:=.4f}",Time1)+'\t'+str.format("{0:<.4f}",Time2)+'\t50.0\t350.0\t'+str.format("{0:<.4f}",EventList[ee][2]))
                            # write feature: FeaMat0 & FeaMat1
                            for hh in range(BinTotal):
                                f.write('\t'+str.format("{0:=.6f}",EventList[ee][3][hh]))
                            for hh in range(BinTotal):
                                f.write('\t'+str.format("{0:=.6f}",EventList[ee][4][hh]))  
                            f.write('\n')
        f.close()
        
if __name__ == "__main__":
    SitePathBase = r'F:\S1068_NZ01_201612_UniformDays'
    VisOutputPath = r'N:\users\yu_shiu_ys587\__SoundScape\__NewZealand'
    
    SiteNameList = os.listdir(SitePathBase)
    
    for SiteName in SiteNameList:
    #for SiteName in SiteNameList[:1]: #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
        #SitePath = r'C:\ASE_Data\__NewZealand\S1068NZ01_S01'
        ##SitePath = r'F:\S1068_NZ01_201612_UniformDays\S1068NZ01_S01'
        #SitePath = r'C:\ASE_Data\__NewZealand\S1068NZ01_S01\S1068NZ01_S01_20161211'
        #SitePath = SitePath.replace('\\','/')
        #SiteName = os.path.split(SitePath)[-1]
        SitePath = os.path.join(SitePathBase, SiteName)
        
        if FLAG_FIG:
            #fig, axarr = plt.subplots(3, 3, sharex=True, sharey=True)
            fig, axarr = plt.subplots(nrows=3, ncols=3, sharex=True, sharey=True, figsize=(18.0, 9.0)) 
            fig.text(0.5, 0.04, 'Time (Hour)', ha='center')
            fig.text(0.04, 0.5, 'Frequency (Hz)', va='center', rotation='vertical')
        
        WorkList = sorted(glob.glob(os.path.join(SitePath+'/','*'))) 
        cc = 0
        for dd in range(len(WorkList)): # days in a single site
        #for dd in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
            WorkList[dd] = WorkList[dd].replace('\\','/')
            print WorkList[dd]
            DayList = sorted(glob.glob(os.path.join(WorkList[dd]+'/','*')))
            
            m = regex.search(os.path.split(WorkList[dd])[-1])
            NameYYYYMMDD = m.groups()[0]
        
            #t1 = time.time()
            #DaySpectrogramAbs = abs(DaySpectroCalc(DayList[0]))
            
            print "Calculating spectrogram..."
            DaySpectrogramAbs = []
            #for ii in range(2): #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
            for ii in range(len(DayList)): # sound files in a single day
                #if ii % 10 == 0:
                if ii % 1 == 0:                    
                    print "Sound file from " + str(ii) + "..."
                #DaySpectrogramAbs.append(DaySpectroCalc(DayList[ii]))
                #DaySpectrogramAbs.append(DaySpectroCalcSimple(DayList[ii], TimeReso))
                DaySpectrogramAbs.append(DaySpectroCalcCQTSimple(DayList[ii], TimeReso))
                    
            print "Merging..."
            DaySpectrogramAbsTot = np.hstack(DaySpectrogramAbs)
            del DaySpectrogramAbs
            print "DaySpectrogramAbsTot shape: " + str(DaySpectrogramAbsTot.shape)
            print "DaySpectrogramAbsTot T shape: " + str((DaySpectrogramAbsTot.T).shape)
            
            if FLAG_FIG:
                #axarr[dd/2, dd%2].imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect='equal')
                Fs = sfile.info(DayList[dd]).samplerate
                #XYLim = [0.0, 24.0, 0.0, FsRef/2.0]
                #XYLim = [0.0, 24.0, FMin, FMin*(2.**NumOct)]
                #axarr[cc/3, cc%3].imshow(DaySpectrogramAbsTot**.1, origin='lower', extent=XYLim, aspect='auto') #############################<<<<<<<<<<<<<<<<<<<<<<<<<<===================================
                #xx = np.arange(0.0, 24.0)
                
                DimT = DaySpectrogramAbsTot.shape[1]
                xx = np.arange(0, DimT)*TimeReso*512./Fs/3600
                yy = FMin*2**(np.arange(0, (NumOct*1.0)*MidiPerOct)/MidiPerOct)
                axarr[cc/3, cc%3].pcolor(xx, yy, (DaySpectrogramAbsTot)**.1)
                axarr[cc/3, cc%3].set_yscale('log')
                axarr[cc/3, cc%3].set_title(NameYYYYMMDD)
                cc += 1
    
        #plt.show()

        #fig.savefig(os.path.split(SitePath)[-1]+".png") ##<<=== need add time
        #fig.savefig(SiteName+".png")
        # Colorbar
        
        #fig.subplots_adjust(right=0.8)
        #cbar_ax = fig.add_axes([0.85, 0.15, 0.05, 0.7])
        #fig.colorbar(im, cax=cbar_ax)
        fig.savefig(os.path.join(VisOutputPath, SiteName+".png"))
    
    #MidFreqArr = MidFreqTable(20, 32) #
    #BandEdgeArr = BandEdgeTable(MidFreqArr)
    #FilterBankList = FilterBank(BandEdgeArr, Fs)
    #Meta = [FilterBankList, SegLen, TimeResolution]
        
    #print str(time.time()-t1)+ ' sec'
    
    #LongTermSpectrogram = LongTermSpectrogram(DayList)
                     
    if False:
        fig, axarr = plt.subplots(2, 2, sharex=True, figsize=(80, 60))
        axarr[0, 0].imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect='equal')
        #ax.imshow(DaySpectrogramAbsTot**.1, origin='lower', aspect=.1)
        plt.show()
        #ax.colorbar()
        #ax.imshow(DaySpectrogramAbs, origin='lower', aspect=1. )

    
    
    
#    if TEST_SNGLE_FILE == True:
#        #OnsetDetection(DayList[11], SelTabPathList[11]) # kp11_20160430_000000    
#        #HarmonicsDetect(DayList[0], SelTabPathList[0]) # kp01_20141221_000000
#        ReturnedStr = LongTermSpectrogram(DayList[1], SelTabPathList[1]) #
#        print ReturnedStr


#    else:
#        MyPool = Pool(3) 
#        try:
#            #for ReturnedStr in MyPool.imap_unordered(universal_worker, pool_args(HarmonicsDetect, DayList, SelTabPathList)):
#            #for ReturnedStr in MyPool.imap(universal_worker, pool_args(HarmonicsDetect, DayList, SelTabPathList)):
#            for ReturnedStr in MyPool.imap(PyBaleen.universal_worker, PyBaleen.pool_args(HarmonicsDetect, DayList[1:4], SelTabPathList[1:4])):
#                print ReturnedStr
#            #print "Waiting 10 seconds"
#            time.sleep(10)
#        except KeyboardInterrupt:
#            print "Caught KeyboardInterrupt, terminating workers"
#            MyPool.terminate()
#            MyPool.join()
#        else:
#            print "Work is finished. Quitting gracefully"
#            MyPool.close()
#            MyPool.join()


#def SignalFreqTime(DaySound, Meta):
#    FilterBank = Meta[0]
#    PageSize = Meta[1]
#    TimeReso = Meta[2]
#    # apply filter; calculate power on time segments
#    # PageSize needs to be integer multiple of TimeReso.
#    # Eg. PageSize = 10 min whereas TimeReso = 10 sec 
#    # Iteration over Samples or over filters?
#    Fs = sfile.info(DaySound).samplerate
#    NumTime = int(np.floor(float(sfile.info(DaySound).duration)/TimeReso))
#    #Samples0, Fs = sfile.read(DaySound)
#    NumPerPage = int(PageSize/TimeReso)
#    NoiseFreqTimeDayMap = np.zeros((len(FilterBank), NumTime ))
#    
#    tt = 0
#    for Samples0 in sfile.blocks(DaySound, blocksize = int(PageSize*Fs)):
#        #for ii in range(len(FilterBank)):
#        for ii in range(1):
#            Samples = sig.filtfilt(FilterBank[ii][0], FilterBank[ii][1], Samples0)
#            #for jj in range(NumPerPage):
#            for jj in range(1):
#                NoiseFreqTimeDayMap[ii, tt*NumPerPage+jj:(tt+1)*NumPerPage] = ((Samples[jj*TimeReso:(jj+1)*TimeReso]**2.).mean())**.5
#        tt += 1
        