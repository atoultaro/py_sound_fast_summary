# -*- coding: utf-8 -*-
"""
1. Change the TIME with respect to the start time of the deployment, instead of that of the day
2. Change the PATH of soundfiles to network drive 159 instead of local external hard drive
3. Change the FREQUENCY to 0 and Nyquiste frequency

Created on Wed Oct 04 12:14:19 2017

@author: ys587
"""
# Info

# New path
# P:\projects\2016_BRP_IthacaNY_S1068\S1068_NZ01_201612\S1068_NZ01_TARU_WAV\S1068NZ01_S02\S1068NZ01_S02_20161210

# Old path
# G:\S1068_NZ01_201612_UniformDays\S1068NZ01_S02\S1068NZ01_S02_20161210

# 
