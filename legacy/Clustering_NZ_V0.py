# -*- coding: utf-8 -*-
"""
k-means clustering on SSW
and also draw

mini-batch

Created on Thu Jun 22 11:08:16 2017

@author: ys587
"""
import glob, os
import numpy as np
import pandas as pd
import logging
from numpy.random import RandomState
import time
from sklearn.cluster import MiniBatchKMeans
import matplotlib.pyplot as plt
import sys
import re
import datetime as dt
fmt = '%Y%m%d'
import matplotlib.dates as mdates

#regexSite = re.compile("S(\d{2})_SWIFT")
regexSite = re.compile("NZ01_S(\d{2})_2")
#regexDay = re.compile("S\d{2}_SWIFT\d{2}_(\d{8})")
regexDay = re.compile("_S\d{2}_(\d{8})")


def ReadSelFeaSite(DayList, DayListFea, DeciRatio, DeciPhase): #def ReadSelFea(WorkPath, DeciRatio, DeciPhase):
    #DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
    #DayListFea = sorted(glob.glob(os.path.join(WorkPath+'/','*_Hog.npy')))
    SelMetaCurr = []
    SelFeaCurr = []
    #for dd in range(len(DayList)):
    DayInd = 0
    for dd in range(DeciPhase, len(DayList), DeciRatio):
    #for dd in range(5,10):
        if dd % 20 == 0:
            logging.warning('Day ' + str(dd)+' '+os.path.basename(DayList[dd]))
        PdCurr = pd.read_csv(DayList[dd], delimiter='\t')
        if PdCurr.shape[0] > 0:
            #PdCurr['Day'] = DayInd
            PdCurr['Date'] = dt.datetime.strptime(regexDay.search(os.path.basename(DayList[dd])).groups()[0], fmt)
            PdCurr['Site'] = int(regexSite.search(os.path.basename(DayList[dd])).groups()[0])-1
            SelMetaCurr.append(PdCurr)
            SelFeaCurr.append(np.load(DayListFea[dd]))
            #logging.warning(np.load(DayListFea[dd]).shape)
        DayInd += 1
        
    SelMetaTot = pd.concat(SelMetaCurr) # merge dataframe
    del SelMetaCurr
    SelMetaTot = SelMetaTot.reset_index() # reset index

    SelFeaTot = np.vstack(SelFeaCurr)
    del SelFeaCurr
    
    return SelMetaTot, SelFeaTot

def DielPlotSiteSoundCount(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf, StartDay):
    SoundCount = np.zeros([NumOfTimeSlots, NumDayYear, NumSite])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            logging.warning(ee)
        TimeStart = SelDf['Begin Time (s)'].iloc[ee]
        DayCurr = SelDf['YearDay'].iloc[ee] - StartDay
        SiteCurr = SelDf['Site'].iloc[ee]
        try:
            SoundCount[int(np.floor(TimeStart/NumOfSec)), int(DayCurr), int(SiteCurr)] += 1.0
        except:
            #print
            logging.warning(ee)
    return SoundCount

def MagAdjustedFea(SelFea, NumFreqBand): # reduce SelFea dimensions and put magnitue as weight on hog
    FeaMag = SelFea[:,-NumFreqBand:]
    SelFea = SelFea[:,:-NumFreqBand]
    
    for ss in range(SelFea.shape[0]):
        for tt in range(NumFreqBand):
            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
            SelFea[ss,tt*NumBin:(tt+1)*NumBin] = SelFea[ss,tt*NumBin:(tt+1)*NumBin]*FeaMag[ss, tt]
    return SelFea

def SoundClipRead(SoundPath, TimeStart, TimeDelta):
    Fs = sf.info(SoundPath).samplerate
    f = sf.SoundFile(SoundPath, 'r')
    f.seek(int(TimeStart*Fs))
    SamplesCall = f.read(int(TimeDelta*Fs))
    return SamplesCall

def Dataframe2SelTab(DFInput, SelTabPath):
    f = open(SelTabPath,'w')
    f.write('Selection\tView\tChannel\tBegin Time (s)\tEnd Time (s)\tLow Freq (Hz)\tHigh Freq (Hz)\tScore\tBegin Path\tFile Offset (s)\tClass\tSite\n')
    for index, row in DFInput.iterrows():
        f.write(str(row[u'Selection'])+'\t'+'Spectrogram'+'\t'+str(row['Channel']+1)+'\t'+ \
                str.format("{0:=.4f}",row[u'Begin Time (s)']) + '\t' + str.format("{0:<.4f}",row[u'End Time (s)'])+ \
                '\t200.0\t800.0\t'+str.format("{0:<.4f}",row[u'Score'])+'\t'+row[u'Begin Path']+ \
                '\t'+str.format("{0:=.4f}",row[u'File Offset (s)'])+'\t'+str.format("{0:=.4f}",row[u'Class'])+'\t'+str.format("{0:=.4f}",row[u'Site'])+'\n')
    f.close()
    return True

def ClassSiteHist(NumSite, NumClass, SelDf):
    SoundClassHist = np.zeros([NumClass, NumSite])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            print ee
        SiteCurr = int(SelDf['Site'].iloc[ee])
        ClassCurr = int(SelDf['Class'].iloc[ee])
        SoundClassHist[ClassCurr, SiteCurr] += 1
    return SoundClassHist

def ClassDayHist(NumDay, NumClass, SelDf, StartDay):
    SoundClassHist = np.zeros([NumClass, NumDay])
    for ee in range(SelDf.shape[0]):
        if ee % 100000 == 0:
            print ee
        #SiteCurr = int(SelDf['Site'].iloc[ee])
        DayCurr = SelDf['YearDay'].iloc[ee] - StartDay
        ClassCurr = int(SelDf['Class'].iloc[ee])
        SoundClassHist[ClassCurr, DayCurr] += 1
    return SoundClassHist

#SoundClassDayHist = ClassDayHist(NumDay, NumClass, SelMetaAll)

if __name__ == "__main__":
    #PROJ = 1 # CHAOZ-X
    #PROJ = 2 # NZ
    PROJ = 4 # NZ. HOG_V2
    #PROJ = 3 # SSW

    #DumpPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__SSW'
    DumpPath = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand'
    
    #WorkPath1 = r'/Volumes/Porter/__ASA_Boston/__SSW/S1067_SSW03_201702_Hog'
    #WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand201612'
    #WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand20171001'
    WorkPath1 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__NewZealand\NewZealand20171012'

    WorkPath1 = WorkPath1.replace('\\','/')
    #WorkPath2 = r'/Volumes/Porter/__ASA_Boston/__SSW/S1067_SSW04_201703_Hog'
    #WorkPath2 = r'P:\users\yu_shiu_ys587\__Soundscape\__ASA_Boston\__SSW\S1067_SSW04_201703_Hog'
    #WorkPath2 = WorkPath2.replace('\\','/')

    SegLength = 30 # in min        
    NumOfTimeSlots = int(24*(60./SegLength))
    NumOfSec = SegLength*60
        
    if PROJ == 1: #CHAOXZ: 8 * 18 + 8
        NumFreqBand = 8
        NumBin = 18
        FFTSize = 512
        HopSize = 200
        NumClass = 100
    elif PROJ == 2:
        NumFreqBand = 10
        NumBin = 18
        NumClass = 200
        NumSite = 10
    elif PROJ == 3:
        NumFreqBand = 8
        NumBin = 18
        FFTSize = 512
        HopSize = 200
        NumClass = 100
        NumSite = 30
    elif PROJ == 4: # From Swift_NZ_EI_HOG_V2
        NumFreqBand = 20 # NumCellTime:2 ; NumCellFreq: 10
        NumBin = 18
        NumClass = 1000
        NumSite = 10
    
    # Read day folders path
    DayList1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*.txt')))
    DayListFea1 = sorted(glob.glob(os.path.join(WorkPath1+'/','*_Hog.npy')))
    #DayList2 = sorted(glob.glob(os.path.join(WorkPath2+'/','*.txt')))
    #DayListFea2 = sorted(glob.glob(os.path.join(WorkPath2+'/','*_Hog.npy')))

    ## Read selection tables & features
    # Month 1
    logging.warning('Late Feb to Early Apr, 2017')
    #SelMetaTot1, SelFeaTot1 = ReadSelFeaSite(DayList1, DayListFea1, 30, 0)
    SelMetaTot1, SelFeaTot1 = ReadSelFeaSite(DayList1, DayListFea1, 1, 0) # read all
    #SelMetaTot2, SelFeaTot2 = ReadSelFeaSite(DayList2, DayListFea2, 30, 0)
    
    logging.warning(' Revising features...')
    SelFeaTot = SelFeaTot1
    #SelFeaTot = np.vstack([SelFeaTot1, SelFeaTot2])
    del SelFeaTot1
    SelMetaTot = SelMetaTot1
    #SelMetaTot = pd.concat([SelMetaTot1, SelMetaTot2])
    del SelMetaTot1
    SelFeaTot = MagAdjustedFea(SelFeaTot, NumFreqBand)
    
    # load and classify
    # Clustering via Mini-batch
    logging.warning(' Minibatch kmeans...')
    
    rng = RandomState(0)
    TimeStart = time.time()
    
    ## Minibatch kmeans
    ##NumClass = 1000 # temporary change
    #MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=rng)
    MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=0)
    #MiniBatchClass.fit(SelMetaTrain[:,:48])
    
    MiniBatchClass.fit(SelFeaTot)
    train_time = (time.time() - TimeStart)
    print("done in %0.3fs" % train_time)
    # get the centroid
    FeaComponents = MiniBatchClass.cluster_centers_
    FeaCounts = MiniBatchClass.counts_

    # prediction on Year 1 & 2
    ClassPred1 = MiniBatchClass.predict(SelFeaTot)
    SelMetaTot['Class'] = ClassPred1
                
    SelMetaAll = SelMetaTot
    del SelMetaTot
    
#    logging.warning('Reading feature from Year 1 for prediction')
#    #SelMetaTest1, SelFeaTest1 = ReadSelFea(WorkPath1, 30, 11)
#    #SelMetaTest1, SelFeaTest1 = ReadSelFea(WorkPath1, 1, 0)
#    #DayList1 = DayList1[:-1]
#    #DayListFea1 = DayListFea1[:-1]
#    SelMetaTest1, SelFeaTest1 = ReadSelFeaSite(DayList1, DayListFea1, 1, 0)
#    #SelMetaTest1, SelFeaTest1 = ReadSelFeaSite(DayList1, DayListFea1, 30, 0)
#
#    # HOG features adjusted by magnitude
#    logging.warning(' Revising features...')
#    SelFeaTest1 = MagAdjustedFea(SelFeaTest1, NumFreqBand)
#    
#    # Prediction
#    logging.warning('Prediction for year 1')    
#    ClassPred1 = MiniBatchClass.predict(SelFeaTest1)
#    SelMetaTest1['Class'] = ClassPred1
#    #SelMetaAll = pd.concat([SelMetaTest1,SelMetaTest2]) # !!
#    #del SelMetaTest1, SelMetaTest2
#    SelMetaAll = SelMetaTest1 # !!
#    del SelMetaTest1
    
    # histogram
    HistCount1, HistBin1 = np.histogram(ClassPred1, bins=np.arange(-.5,float(NumClass)+.5, 1.0))
    plt.figure(); plt.plot(HistBin1[:-1]+0.5, HistCount1,'-+'); plt.grid(); plt.show()


#    # show sound count                    
#    SelMetaTest1[r'YearDay'] = SelMetaTest1[u'Date'].dt.dayofyear
#    SelMetaTest2[r'YearDay'] = SelMetaTest2[u'Date'].dt.dayofyear
#    NumDayMonth1 = SelMetaTest1[r'YearDay'].max() - SelMetaTest1[r'YearDay'].min()
#    StartDayMonth1 = SelMetaTest1[r'YearDay'].min()
#    NumDayMonth2 = SelMetaTest2[r'YearDay'].max() - SelMetaTest2[r'YearDay'].min()
#    StartDayMonth2 = SelMetaTest2[r'YearDay'].min()
#    
#    SoundCount1 = DielPlotSiteSoundCount(NumOfTimeSlots, NumDayMonth1, NumOfSec, SelMetaTest1, StartDayMonth1)
#    SoundCount2 = DielPlotSiteSoundCount(NumOfTimeSlots, NumDayMonth2, NumOfSec, SelMetaTest2, StartDayMonth2)

    # Sound count
    SelMetaAll[r'YearDay'] = SelMetaAll[u'Date'].dt.dayofyear
    StartDay = SelMetaAll[r'YearDay'].min()
    NumDay = SelMetaAll[r'YearDay'].max() - SelMetaAll[r'YearDay'].min() + 1
    SoundCount = DielPlotSiteSoundCount(NumOfTimeSlots, NumDay, NumOfSec, SelMetaAll, StartDay) # time, day, site
    DateStart = dt.datetime(2016, 12, 10)
    DateStop = dt.datetime(2016, 12, 19)
    #DateStop = dt.datetime(2016, 12, 16 )
    
    # show sound count time vs site, omitting days
    fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    #XYLim = [StartDay, StartDay+NumDay, 0, 24]
    XYLim = [0, NumSite, 0, 24]
    #im = axis1.imshow(SoundCount[:,dd,:], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=600, vmin=0)
    t3=SoundCount.sum(axis=1)
    im = axis1.imshow(t3, extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=t3.max(), vmin=0)

    #axis1.set_xticklabels([])    
    #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    #axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    #axis1.xaxis_date()
    axis1.autoscale_view()
    axis1.xaxis.tick_bottom()
    #plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    axis1.set_title('New Zealand: 10-Dec-2016 - 18-Dec-2016')
    axis1.set_xlabel('Site')
    axis1.set_ylabel('Hour of the Day')
    plt.show()


    # show sound count time vs days, omitting sites
    #DateStart = dt.datetime(2016, 12, 10)
    #DateStop = dt.datetime(2016, 12, 18)
    fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, 24]
    #im = axis1.imshow(SoundCount[:,dd,:], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=600, vmin=0)
    SoundCountAllSites = SoundCount.sum(axis=2)
    im = axis1.imshow(SoundCountAllSites, extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax= SoundCountAllSites.max(), vmin=0)

    axis1.set_xticklabels([])    
    #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    axis1.xaxis_date()
    axis1.autoscale_view()
    axis1.xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    #axis1.set_title('SSW: 25-Feb-2017 - 15-Apr-2017')
    axis1.set_title('New Zealand: 10-Dec-2016 - 18-Dec-2016')
    axis1.set_ylabel('Hour of the Day')
    plt.show()


    # show sound count sites vs days
    #DateStart = dt.datetime(2016, 12, 10)
    #DateStop = dt.datetime(2016, 12, 18)
    
    fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, NumSite]
    #im = axis1.imshow(SoundCount[:,dd,:], extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=600, vmin=0)
    t2=SoundCount.sum(axis=0)
    im = axis1.imshow(t2.T, extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=t2.max(), vmin=0)

    axis1.set_xticklabels([])    
    #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    axis1.xaxis_date()
    axis1.autoscale_view()
    axis1.xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    #axis1.set_title('SSW: 25-Feb-2017 - 15-Apr-2017')
    axis1.set_title('New Zealand: 10-Dec-2016 - 18-Dec-2016')
    axis1.set_ylabel('Site')
    plt.show()
    
    
    # show sound class vs sites
    SoundClassSiteHist = ClassSiteHist(NumSite, NumClass, SelMetaAll)
    
    fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    #XYLim = [StartDay, StartDay+NumDay, 0, 24]
    XYLim = [0, NumSite, 0, NumClass]
    im = axis1.imshow(SoundClassSiteHist, extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=SoundClassSiteHist.max(), vmin=0)

    #axis1.set_xticklabels([])    
    #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    #axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    #axis1.xaxis_date()
    axis1.autoscale_view()
    axis1.xaxis.tick_bottom()
    #plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    #axis1.set_title('SSW: 25-Feb-2017 - 15-Apr-2017')
    axis1.set_title('New Zealand: 10-Dec-2016 - 18-Dec-2016')
    axis1.set_xlabel('Site')
    axis1.set_ylabel('Sound Class')
    plt.show()
    
    # show sound class vs days
    SoundClassDayHist = ClassDayHist(NumDay, NumClass, SelMetaAll, StartDay)

    fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    #XYLim = [StartDay, StartDay+NumDay, 0, 24]
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, NumClass]
    im = axis1.imshow(SoundClassDayHist, extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=SoundClassDayHist.max(), vmin=0)

    axis1.set_xticklabels([])    
    #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    axis1.xaxis_date()
    axis1.autoscale_view()
    axis1.xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    #axis1.set_title('SSW: 25-Feb-2017 - 15-Apr-2017')
    axis1.set_title('New Zealand: 10-Dec-2016 - 18-Dec-2016')
    #axis1.set_xlabel('Site')
    axis1.set_ylabel('Sound Class')
    plt.show()
    
    
    # Given class, find the audio clips and write them into a selection table
    if False:
        ClassTarget = 303 # 1st peak at 65; 2nd peak at 25
        SelMetaTarget = SelMetaAll.loc[SelMetaAll['Class'] == ClassTarget]
        SelTabPath = os.path.join(DumpPath,'NZ_Class'+str(ClassTarget)+'.txt')
        Dataframe2SelTab(SelMetaTarget.sample(200), SelTabPath)



    ###################################################### <<== TO DO!!
    # Given a class, class vs Site
    
    ###################################################### <<== TO DO!!
    # show sound class vs days -- two groups of classes
    SoundClassDayHist = ClassDayHist(NumDay, NumClass, SelMetaAll, StartDay)

    fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    #XYLim = [StartDay, StartDay+NumDay, 0, 24]
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, NumClass]
    im = axis1.imshow(SoundClassDayHist, extent=XYLim, origin='lower', aspect='auto', cmap='jet', vmax=SoundClassDayHist.max(), vmin=0)

    axis1.set_xticklabels([])    
    #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b'))
    axis1.xaxis_date()
    axis1.autoscale_view()
    axis1.xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Count of Sound Event', fontdict={'fontsize':10})
    
    #axis1.set_title('SSW: 25-Feb-2017 - 15-Apr-2017')
    axis1.set_title('New Zealand: 10-Dec-2016 - 18-Dec-2016')
    #axis1.set_xlabel('Site')
    axis1.set_ylabel('Sound Class')
    plt.show()
    ######################################################
    
    
    ## TO-DO Sept 6, 2017
    # 1. Replace path F:/S1068_NZ01_201612_UniformDays/S1068NZ01_S10 by 
    # P:\projects\2016_BRP_IthacaNY_S1068\S1068_NZ01_201612\S1068_NZ01_TARU_FLAC
    # 2. change slash / by backslash \
    
    if False:
        #SelMetaAll2 = SelMetaAll
        for ii in range(SelMetaAll.shape[0]):
             t1 = re.sub(r'[\/]', r'\\', SelMetaAll.loc[ii]['Begin Path'])
             t2 = re.sub(r'F:\\S1068_NZ01_201612_UniformDays', r'P:\projects\\2016_BRP_IthacaNY_S1068\S1068_NZ01_201612\\S1068_NZ01_TARU_FLAC' ,t1)
             # SelMetaAll2.loc[ii]['Begin Path']
             SelMetaAll.set_value(ii ,'Begin Path', t2 )

    #sys.exit()    
    
##    ## Sound Class: class hist vs day
#    SoundClassHist1 = ClassHistPerDay(NumOfTimeSlots, NumDayYear1, NumOfSec, SelMetaTest1, NumClass)
#    SoundClassHist2 = ClassHistPerDay(NumOfTimeSlots, NumDayYear2, NumOfSec, SelMetaTest2, NumClass)
#    #
#    VMax = max(SoundClassHist1.flatten().max(), SoundClassHist2.flatten().max())
#    VMax = 0.2*VMax
#    Colormap0 = 'jet'
#    fig5, axis5 = plt.subplots(nrows=2, ncols=1, figsize=[18, 12])
#    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 1, 100]
#    im = axis5[0].imshow(SoundClassHist1[:,0:350], extent=XYLim, origin='lower', cmap=Colormap0, vmin=0, vmax=VMax)
#    im = axis5[1].imshow(SoundClassHist2[:,21:371], extent=XYLim, origin='lower', cmap=Colormap0, vmin=0, vmax=VMax)
#    
#    axis5[0].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
#    axis5[0].set_xticklabels([])    
#    axis5[1].xaxis.set_major_locator(mdates.MonthLocator( interval=1))
#    axis5[1].xaxis.set_major_formatter(mdates.DateFormatter('%d-%b-%Y'))
#    axis5[1].xaxis_date()
#    axis5[1].autoscale_view()
#    axis5[1].xaxis.tick_bottom()
#    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)
#    
#    fig5.subplots_adjust(right=0.8)
#    cbar_ax = fig5.add_axes([0.85, 0.15, 0.05, 0.7])
#    cb5 = fig5.colorbar(im, cax=cbar_ax)
#    cb5.ax.set_title('Histogram of Sound Classes', fontdict={'fontsize':10})
#    
#    axis5[0].set_title('DB13_01: 20-Aug-2013 - 04-Aug-2014')
#    axis5[0].set_ylabel('Class Index')
#    axis5[1].set_title('DB14_01: 20-Aug-2014 - 04-Aug-2015')
#    axis5[1].set_ylabel('Class Index')
#    plt.show()
    
    
#    FeaMagTest = SelFeaTest[:,-NumFreqBand:]
#    SelFeaTest = SelFeaTest[:,:-NumFreqBand]
#    
#    for ss in range(SelFeaTest.shape[0]):
#        for tt in range(NumFreqBand):
#            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
#            SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin]*FeaMagTest[ss, tt]
            
#    FeaMagTest = SelFeaTest[:,-NumFreqBand:]
#    SelFeaTest = SelFeaTest[:,:-NumFreqBand]
#    
#    for ss in range(SelFeaTest.shape[0]):
#        for tt in range(NumFreqBand):
#            #SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTot[ss,tt*NumBin:(tt+1)*NumBin]*SelFeaTot[ss,NumFreqBand*NumBin+tt]
#            SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin] = SelFeaTest[ss,tt*NumBin:(tt+1)*NumBin]*FeaMagTest[ss, tt]
    
## Decomposition
#    if False:
#        logging.warning(' Minibatch dictionary learning...')
#        NumDict = 100
#        from sklearn.decomposition import MiniBatchDictionaryLearning
#        MiniBatchDict = MiniBatchDictionaryLearning(n_components=NumDict)
#        MiniBatchDict.fit(SelFeaTot)
#        # plt.plot(MiniBatchDict.components_[10],'-+'); plt.grid()
#        DictComp = MiniBatchDict.transform(SelFeaTot) # how about using those dictionary items to do kmeans?
    
    
    # read both selection tables and features
#    DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
#    DayListFea = sorted(glob.glob(os.path.join(WorkPath+'/','*_Hog.npy')))
#    SelMetaCurr = []
#    SelFeaCurr = []
#    #for dd in range(len(DayList)):
#    for dd in range(1, len(DayList), 4):
#    #for dd in range(5,10):
#        logging.warning('Day' + str(dd))
#        SelMetaCurr.append(pd.read_csv(DayList[dd], delimiter='\t'))
#        SelFeaCurr.append(np.load(DayListFea[dd]))
#        
#    SelMetaTot = pd.concat(SelMetaCurr) # merge dataframe
#    del SelMetaCurr
#    SelMetaTot = SelMetaTot.reset_index() # reset index
#
#    SelFeaTot = np.vstack(SelFeaCurr)
#    del SelFeaCurr

