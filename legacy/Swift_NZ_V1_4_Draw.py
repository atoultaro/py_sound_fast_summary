# -*- coding: utf-8 -*-
"""
2 problems:
1. 75619
2. 01_1213
Created on Mon May 01 14:43:32 2017

@author: ys587
"""

import pandas as pd
import glob
import os
import numpy as np
import matplotlib.pyplot as plt
import time
from numpy.random import RandomState
from sklearn.cluster import MiniBatchKMeans
#import collections
#from sklearn.manifold import MDS
#from collections import Counter

def ReadMetaFea(DayInd, DataPath, ColSetInfo):
    # Read from files
    SelData = np.genfromtxt(DataPath, delimiter='\t')
    SelData = SelData[1:,:]
    # Extract the meta data    
    ##SelDataMeta0 = SelData[1:, ColSetInfo]
    SelDataMeta0 = SelData[:, ColSetInfo]
    #print SelDataMeta0[:,3].max()
    
    # Extract the meta data
    ScoreThre = np.percentile(SelDataMeta0[:, 4+2], [0])[0]

    SelInd0 = (SelDataMeta0[:, 4+2] > ScoreThre) * (SelDataMeta0[:, 1] == 1.0)
    #print SelInd0.sum()
    
    #SelFea0 = SelData[SelInd0, 8:8 + 36]
    
    #SelIndNan = np.where((np.isnan(SelFea0).sum(axis=1))> 0 )[0]
    SelIndNan = np.where((np.isnan(SelData[:, 8:8 + 36]).sum(axis=1))> 0 )[0]
    for nn in SelIndNan:
        SelInd0[nn] = False
        print 'nan on ' + str(nn)
    
    #SelDataMeta1 = SelDataMeta0[SelInd1[SelInd], :]
    #SelDataMeta1 = SelDataMeta0[SelInd1[1:], :]
    SelDataMeta1 = SelDataMeta0[SelInd0, :]
    
    SelDataMeta = np.c_[np.ones(SelDataMeta1.shape[0])*DayInd, SelDataMeta1]
        
    #SelFea = SelFea0[SelInd1, :]
    #SelFea = SelData[SelInd1, :]
    SelFea = SelData[SelInd0, 8:8 + 36]
    
    return SelDataMeta, SelFea

def SoundCountVsTime(NumSite, NumDay, NumOfSec, SelDf):
    NumTimeSlots = int(np.floor(NumDay*86400./NumOfSec))
    
    SoundCount = np.zeros([NumSite, NumTimeSlots])
    for ee in range(SelDf.shape[0]):
        if ee % 1000 == 0:
            print ee
        SiteCurr = SelDf['Site'][ee]
        DayCurr = SelDf['DayReal'][ee]
        TimeStart = SelDf['TimeStart'][ee]
        IndTime = int(np.floor(((DayCurr-1)*86400.+TimeStart)/NumOfSec))
        if IndTime < NumTimeSlots:
            SoundCount[int(SiteCurr)-1, IndTime] += 1.0
    return SoundCount

def SoundClassVsTime(NumSite, NumDay, NumOfSec, NumClass, SelDf):
    NumTimeSlots = int(np.floor(NumDay*86400./NumOfSec))
    
    SoundClassCount = np.zeros([NumSite*NumClass, NumTimeSlots])
    for ee in range(SelDf.shape[0]):

        SiteCurr = SelDf['Site'][ee]
        TimeStart = SelDf['TimeStart'][ee]
        DayCurr = SelDf['DayReal'][ee]
        ClassCurr = SelDf['Class'][ee]
        IndTime = int(np.floor(((DayCurr-1)*86400.+TimeStart)/NumOfSec))
        if IndTime < NumTimeSlots:
            SoundClassCount[(int(SiteCurr)-1)*NumClass+ClassCurr, IndTime] += 1.0
            #print (SiteCurr-1.)*NumClass+ClassCurr
        else:
            print "IndTime > NumTimeSlots"
            print IndTime
    return SoundClassCount

def SoundClassDominantVsTime(NumSite, NumDay, NumOfSec, NumClass, SelDf):
    NumTimeSlots = int(np.floor(NumDay*86400./NumOfSec))
    
    SoundClassCount = np.zeros([NumSite*NumClass, NumTimeSlots])
    SoundClassDominant = np.zeros([NumSite, NumTimeSlots])
    for ee in range(SelDf.shape[0]):
        SiteCurr = SelDf['Site'][ee]
        TimeStart = SelDf['TimeStart'][ee]
        DayCurr = SelDf['DayReal'][ee]
        ClassCurr = SelDf['Class'][ee]
        IndTime = int(np.floor(((DayCurr-1)*86400.+TimeStart)/NumOfSec))
        if IndTime < NumTimeSlots:
            SoundClassCount[(int(SiteCurr)-1)*NumClass+ClassCurr, IndTime] += 1.0
        else:
            print "IndTime > NumTimeSlots"
            print IndTime
    for tt in range(NumTimeSlots):
        for ss in range(NumSite):
            SoundClassCount[ss, tt] = np.argmax(SoundClassCount[ss*NumSite:(ss+1)*NumSite,tt])
            
    return SoundClassDominant

#def ClassHistPerDay(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf, NumClass):
#    SoundClassHist = np.zeros([NumClass, NumDayYear])
#    for ee in range(SelDf.shape[0]):
#        if ee % 1000 == 0:
#            print ee
#        DayCurr = SelDf['Day'][ee]
#        ClassCurr = SelDf['Class'][ee]
#        SoundClassHist[ClassCurr, DayCurr] += 1
#                      
#    return SoundClassHist
#
#def ClassHistPerHour(NumOfTimeSlots, NumDayYear, NumOfSec, SelDf, NumClass):
#    SoundClassHistHour = np.zeros([NumClass, NumOfTimeSlots])
#    for ee in range(SelDf.shape[0]):
#        if ee % 1000 == 0:
#            print ee
#        HourCurr = int(np.floor(SelDf['TimeStart'][ee]/NumOfSec))
#        ClassCurr = SelDf['Class'][ee]
#        SoundClassHistHour[ClassCurr, HourCurr] += 1
#    return SoundClassHistHour

if __name__ == "__main__":
    WorkPath = r'P:\users\yu_shiu_ys587\__Soundscape\__NewZealand\__OnsetSelTable'    
    WorkPath = WorkPath.replace('\\','/')
    DayList = sorted(glob.glob(os.path.join(WorkPath+'/','*.txt')))
    
    NumClass = 20  ### <<< ===
    #NumClassBagOfSound = 100
    SegLength = 30 # in min
        
    NumOfTimeSlots = int(24*(60./SegLength))
    NumOfSec = SegLength*60

    ###############################################################################################
    # Read the detection logs in
    SelFeaList = []
    SelMetaList = []
    
    # Columns we need
    ColSetInfo = [0, 2, 3, 4, 5, 6, 7]
    
    ColSetFea = range(8, 8+36) # 36 features

    # Year 1
    print 'Year 1'
    for dd in range(len(DayList)):
    #for dd in range(12, 13):
    #for dd in range(1, len(DayList), 30): ##<<<===
    #for dd in range(4, len(DayList)):
    #for dd in range(5,10):
    #for dd in range(4):
        print 'Day' + str(dd)
        #ReadMetaFea(dd, DayList[dd], ColSetInfo)
        SelDataMeta, SelFea = ReadMetaFea(dd, DayList[dd], ColSetInfo)
        SelMetaList.append(SelDataMeta)
        SelFeaList.append(SelFea)
    
    # Merge SelMetaList into a dataframe
    SelMetaTrain = np.vstack(SelMetaList)
    del SelMetaList
    SelMetaTrainDf = pd.DataFrame(SelMetaTrain, columns=['Day', 'EventId', 'Channel', 'TimeStart', 'TimeStop', 'FreqLow', 'FreqHigh','Score'])
    #del SelMetaTrain
    # Merge SelFeaList into a feature set
    SelFeaTrain = np.vstack(SelFeaList)
    del SelFeaList
        
    # Clustering via Mini-batch
    if True:
        print "Clustering via Mini-batch"
        rng = RandomState(0)
        TimeStart = time.time()
        MiniBatchClass = MiniBatchKMeans(n_clusters=NumClass, tol=1e-3, batch_size=40, max_iter=100, random_state=rng)
        
        MiniBatchClass.fit(SelFeaTrain)
        train_time = (time.time() - TimeStart)
        print("done in %0.3fs" % train_time)
        # get the centroid
        FeaComponents = MiniBatchClass.cluster_centers_
        
        # load and classify
        print "Classification....."
        ClassPredYear1 = []
        SelMetaList1 = []
        for dd in range(len(DayList)):
            print 'Day' + str(dd)
            SelDataMeta, SelFea = ReadMetaFea(dd, DayList[dd], ColSetInfo)
            SelMetaList1.append(SelDataMeta)
            
            ClassPred = MiniBatchClass.predict(SelFea)
            ClassPredYear1.append(ClassPred)
        
        SelMetaTest1 = np.vstack(SelMetaList1)
        SelMetaTest1Df = pd.DataFrame(SelMetaTest1, columns=['Day', 'EventId', 'Channel', 'TimeStart', 'TimeStop', 'FreqLow', 'FreqHigh','Score'])
        del SelMetaTest1, SelMetaList1
        ClassPredYear1Arr = np.hstack(ClassPredYear1)
        del ClassPredYear1
        
        # Convert "Day" SelMetaTest1Df into "Day" and "Site"
        SelMetaTest1Df['Site'] = np.ceil((SelMetaTest1Df['Day']+1.)/9.)
        SelMetaTest1Df['DayReal'] = (SelMetaTest1Df['Day'] % 9.)+1.
        
        # Draw!
        SelMetaTest1Df['Class'] = ClassPredYear1Arr
        NumDay = int(SelMetaTest1Df['DayReal'].max())
        NumSite = int(SelMetaTest1Df['Site'].max())
        NumOfSec = 30.*60 # 10 min
    
    ######################################################################
    # Sound Count
    if True:
        SoundCount = SoundCountVsTime(NumSite, NumDay, NumOfSec, SelMetaTest1Df)
        
        import datetime as dt
        import matplotlib.dates as mdates    
        
        DateStart = dt.datetime(2016, 12, 10)
        DateStop = dt.datetime(2016, 12, 19)
        XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, 10]
    
        fig1, axis1 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
        im = axis1.imshow(SoundCount, extent=XYLim, origin='upper', aspect='auto', cmap='jet')
        
        #axis1.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
        axis1.set_xticklabels([])    
        axis1.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b-%Y'))
        axis1.xaxis_date()
        axis1.autoscale_view()
        axis1.xaxis.tick_bottom()
        plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)
        
        axis1.grid(b=True, which='major', axis='x', color='k', linestyle='-.')
        
        yticks = ['Site 1', 'Site 2', 'Site 3', 'Site 4', 'Site 5', 'Site 6', 'Site 7', 'Site 8', 'Site 9', 'Site 10']
        plt.yticks(np.arange(9.5, -0.5, -1.0), yticks)
        
        fig1.subplots_adjust(right=0.8)
        cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
        cb1 = fig1.colorbar(im, cax=cbar_ax)
        cb1.ax.set_title('Count of Sound Events', fontdict={'fontsize':10})
        
        axis1.set_title('New Zealand Dataset: 10-Dec-2016 - 18-Dec-2016')
        #axis1.set_ylabel('Site')
        plt.show()
    
    ######################################################################
    # Sound Class Distribution
    if True:
        SoundClassCount = SoundClassVsTime(NumSite, NumDay, NumOfSec, NumClass, SelMetaTest1Df)
        
        DateStart = dt.datetime(2016, 12, 10)
        DateStop = dt.datetime(2016, 12, 19)
        XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, 10]
    
        fig1, axis2 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
        im = axis2.imshow(SoundClassCount, extent=XYLim, origin='upper', aspect='auto', cmap='jet')
        
        #axis2.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
        axis2.set_xticklabels([])    
        axis2.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b-%Y'))
        axis2.xaxis_date()
        axis2.autoscale_view()
        axis2.xaxis.tick_bottom()
        plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)

        axis2.grid(b=True, which='major', axis='x', color='k', linestyle='-.')
        #axis2.grid(b=True, which='minor', axis='x', color='k', linestyle='--')
        
        yticks = ['Site 1', 'Site 2', 'Site 3', 'Site 4', 'Site 5', 'Site 6', 'Site 7', 'Site 8', 'Site 9', 'Site 10']
        plt.yticks(np.arange(9.5, -0.5, -1.0), yticks)        
        axis2.set_title('New Zealand Dataset: 10-Dec-2016 - 18-Dec-2016')

        # Colorbar
        fig1.subplots_adjust(right=0.8)
        cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
        cb1 = fig1.colorbar(im, cax=cbar_ax)
        cb1.ax.set_title('Count of Sound Classes', fontdict={'fontsize':10})
        
        plt.show()
    
    ######################################################################
    # Sound Dominant Class
    #SoundClassDominant = SoundClassDominantVsTime(NumSite, NumDay, NumOfSec, NumClass, SelMetaTest1Df)
    NumTimeSlots = int(np.floor(NumDay*86400./NumOfSec))
    SoundClassDominant = np.zeros([NumSite, NumTimeSlots])
    for tt in range(NumTimeSlots):
        for ss in range(NumSite):
            SoundClassDominant[ss, tt] = np.argmax(SoundClassCount[ss*NumClass:(ss+1)*NumClass,tt])    
    
    DateStart = dt.datetime(2016, 12, 10)
    DateStop = dt.datetime(2016, 12, 19)
    XYLim = [mdates.date2num(DateStart), mdates.date2num(DateStop), 0, 10]

    fig1, axis3 = plt.subplots(nrows=1, ncols=1, figsize=[18, 8])
    im = axis3.imshow(SoundClassDominant, extent=XYLim, origin='upper', aspect='auto', cmap='jet')
    
    #axis3.xaxis.set_major_locator(mdates.MonthLocator( interval=1))
    axis3.set_xticklabels([])    
    axis3.xaxis.set_major_formatter(mdates.DateFormatter('%d-%b-%Y'))
    axis3.xaxis_date()
    axis3.autoscale_view()
    axis3.xaxis.tick_bottom()
    plt.setp(plt.gca().get_xticklabels(), rotation=30, fontsize=10)
    
    axis3.grid(b=True, which='major', axis='x', color='k', linestyle='-', linewidth=3)
    
    yticks = ['Site 1', 'Site 2', 'Site 3', 'Site 4', 'Site 5', 'Site 6', 'Site 7', 'Site 8', 'Site 9', 'Site 10']
    plt.yticks(np.arange(9.5, -0.5, -1.0), yticks)
    
    fig1.subplots_adjust(right=0.8)
    cbar_ax = fig1.add_axes([0.85, 0.15, 0.05, 0.7])
    cb1 = fig1.colorbar(im, cax=cbar_ax)
    cb1.ax.set_title('Dominant Sound Classes', fontdict={'fontsize':10})
    
    axis3.set_title('New Zealand Dataset: 10-Dec-2016 - 18-Dec-2016')
    #axis3.set_ylabel('Site')
    plt.show()
    
    ######################################################################
    # High Frequency plot
    SelMetaDfSite = [SelMetaTest1Df[SelMetaTest1Df['Site']==ss] for ss in np.arange(9.0)+1.0]
    
    